# Proef Diligentia Uitgeverij

Maak aan de hand van docker-compose en minstens 3 containers een 2-delige applicatie. 

## Symfony
Een API waarbij je auteurs, publicaties en (anonieme) recensies kan beheren. Hierbij is voornamelijk belangrijk dat je toont te begrijpen hoe je kan werken met joins en relationele data. De specifieke uitwerking kies je vrij, net als het soort API dat je maakt.

## NGINX
Een simpele NGINX container waar je de frontend-applicatie mee host. Ook hier laten we de invulling vrij. Grafisch mag de applicatie erg barebones zijn, en mag je gewoon default components van een (css-)framework gebruiken. In de applicatie willen we een overzicht van auteurs en een overzicht van publicaties, een detail pagina van een auteur met daarbij een lijst van publicaties, en een detail pagina van een publicatie met daarbij de recensies. 

### Nice To Haves
- publicaties kunnen sorteren alfabetisch / gemiddelde score
- gemiddelde score per publicatie zichtbaar
- auteurs kunnen sorteren alfabetisch / gemiddelde score

## mySQL / Mariadb
De database host.

# Hoe beginnen
Fork deze repo

# Hoe indienen
Maak een pull-request